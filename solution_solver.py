INPUT_FILE_NAME = "input.txt"  # Replace with input file
SOLUTION_FILE_NAME = "solution.txt"  # Replace with solution file


solution_ingredient_count = 0
solution_ingredients = []
total_points = 0

with open(SOLUTION_FILE_NAME) as file:
    lines = file.readlines()
    for line in lines:
        solution_ingredient_count = line.split(" ")[0]
        solution_ingredients = line.split(" ")[1:]


with open(INPUT_FILE_NAME) as file:
    lines = file.readlines()
    line_count = 0
    like_ingredients = []
    dislike_ingredients = []

    # Ignore first line since is just the number of customers
    for line in lines[1:]:
        line_count += 1
        if line_count == 1:
            # Ingredients customer likes
            like_ingredients = line.replace("\n","").split(" ")[1: ]
        if line_count == 2:
            # Ingredients customer is not a fan off
            dislike_ingredients = line.replace("\n","").split(" ")[1: ]
            line_count = 0

            if set(like_ingredients).issubset(solution_ingredients):
                total_points += 1
                for dislike in dislike_ingredients:
                    if dislike in solution_ingredients:
                        total_points -= 1
                        break

print(f"Total points of your solution: {total_points}")